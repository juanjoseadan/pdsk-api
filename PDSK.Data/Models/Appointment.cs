using System;

namespace PDSK.Data.Models
{
	public class Appointment
	{
		public int AppointmentId { get; set; }

		// Could be an enum? Maybe a list of enums and override the get to show it as string?
		public string AppointmentType { get; set; }

		public DateTime CreateDateTime { get; set; }

		public DateTime RequestedDateTimeOffset { get; set; }

		// TODO: Find a way to map this without underscore (not a good practice in C#)
		public int user_UserId { get; set; }
		// TODO: Find a way to map this without underscore (not a good practice in C#)
		public int animal_AnimalId { get; set; }



		public User User { get; set; }
		public Animal Animal { get; set; }
	}
}