using System;

namespace PDSK.Data.Exceptions
{
	public class AppointmentException : Exception
	{
		public AppointmentException(string message) : base(message)
		{
			
		}
	}
}