using System;

namespace PDSK.Data.DTO
{
	public class AppointmentDTO
	{
		public int AppointmentId { get; set; }

		public string OwnerFullName { get; set; }

		public string PetName { get; set; }

		public string PetSpecies { get; set; }

		public string PetBreed { get; set; }

		public string AppointmentType { get; set; }

		public DateTime AppointmentDateTime { get; set; }

		public bool Confirmed { get; set; } = false;

		public int OwnerId { get; set; }
	}
}