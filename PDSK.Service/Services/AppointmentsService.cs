using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using PDSK.Service.Interfaces;
using PDSK.Data.Models;
using PDSK.Data.Exceptions;
using PDSK.Data.DTO;
using PDSK.Service.Extensions;

namespace PDSK.Service.Services
{
	public class AppointmentsService : IAppointmentsService
	{
		private readonly IPetDeskRestClient _httpClient;

		public AppointmentsService(IPetDeskRestClient httpClient)
		{
			_httpClient = httpClient;
		}

		public async Task<List<AppointmentDTO>> GetAppointments()
		{
			try
			{
				var appointments = await _httpClient.Get<List<Appointment>>("api/appointments");
				
				// This is all the info we need in the react app
				return appointments
					.Select(x => new AppointmentDTO
					{
						AppointmentId = x.AppointmentId,
						OwnerFullName = x.User.GetFullName(),
						PetName = x.Animal?.FirstName ?? string.Empty,
						PetSpecies = x.Animal?.Species ?? string.Empty,
						PetBreed = x.Animal?.Breed ?? string.Empty,
						AppointmentType = x.AppointmentType,
						AppointmentDateTime = x.RequestedDateTimeOffset,
						OwnerId = x.user_UserId,
					})
					.OrderBy(x => x.AppointmentDateTime)
					.ToList();
			}
			catch (Exception e)
			{
				throw new AppointmentException($"GetAppointmentsException: {e.Message}");
			}
		}
	}
}