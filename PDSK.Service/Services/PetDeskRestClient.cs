using System.Threading.Tasks;
using PDSK.Service.Interfaces;
using Microsoft.Extensions.Configuration;
using RestSharp;

namespace PDSK.Service.Services
{
	// For more complex situations (i. e. multiple sources) I would have used a Factory Pattern
	public class PetDeskRestClient : IPetDeskRestClient
	{
		private readonly string BaseUrl = string.Empty;


		public PetDeskRestClient(IConfiguration config)
		{
			BaseUrl = config.GetSection("PetDesk:BaseUrl").Value;
		}

		public async Task<T> Get<T>(string path)
		{
			var client = new RestClient(BaseUrl);
			var request = new RestRequest(path, DataFormat.Json);
			var response = await client.GetAsync<T>(request);

			return response;
		}
	}
}