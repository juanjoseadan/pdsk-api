using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PDSK.Data.DTO;

namespace PDSK.Service.Interfaces
{
	public interface IAppointmentsService
	{
		Task<List<AppointmentDTO>> GetAppointments();
	}
}