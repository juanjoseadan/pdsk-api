using System.Threading.Tasks;

namespace PDSK.Service.Interfaces
{
	public interface IPetDeskRestClient
	{
		Task<T> Get<T>(string url);
	}
}