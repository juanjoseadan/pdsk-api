using System;
using PDSK.Data.Models;

namespace PDSK.Service.Extensions
{
	public static class UserExtension
	{
		public static string GetFullName(this User user)
		{
			if (user != null)
			{
				return $"{user.FirstName} {user.LastName}";
			}

			return string.Empty;
		}
	}
}