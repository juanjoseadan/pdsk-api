using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PDSK.Service.Interfaces;
using PDSK.Data.Exceptions;

namespace PDSK.Api.Controllers
{
	[ApiController]
	[Route("api/appointments")]
	public class AppointmentsController : ControllerBase
	{
		private readonly IAppointmentsService _appointmentsService;

		public AppointmentsController(IAppointmentsService appointmentsService)
		{
			_appointmentsService = appointmentsService;
		}

		[HttpGet("")]
		public async Task<IActionResult> GetAppointments()
		{
			try
			{
				var appointments = await _appointmentsService.GetAppointments();
				
				return Ok(appointments);
			}
			catch (AppointmentException e)
			{
				return StatusCode(500, e);
			}
		}
	}
}