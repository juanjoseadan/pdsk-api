# PetDesk - Demo API

## Clone the repository
Run the next command in your git batch if you don't have the repository locally: `git clone https://juanjoseadan@bitbucket.org/juanjoseadan/pdsk-api.git`

**Note:** the branch containing the latest changes is _master_

## Steps for running API locally (CLI)

1. Open a Terminal/Command Line and open the root folder of this project.
2. Run the next command to restore the NuGet packages: `dotnet restore`. Wait until the command finishes.
3. Run the API locally by executing the next command: `dotnet run -p PDSK.Api/`. Make sure you are still in the root folder of this project.
4. Everything should be good from now.


**Note:** this API is configured to run under `http://localhost:5001` so if you get an error in step **#3**, you might have port 5001 already in use.


## Steps for running API locally (Visual Studio)
1. Open the `PDSK.sln` file in your Visual Studio
2. Set up the `PDSK.Api` project as the _Startup project_:
	- Right click in the Solution name and click "Properties"
	- Go to the Web tab and select `PDSK.Api` as your _Startup project_